#include <iostream>
#include <string>
using namespace std;
struct barang {
    char kd_brg[12], nm_brg[40], size[10], tglmsk[40];
    int hrg_beli, hrg_jual, stok;
};

int i;
bool ulang;

int main() {
    barang brg[100]{};
    int k = 1;
    cout << "=========================================================" << endl;
    cout << "                     Tampilan Input                      " << endl;
    cout << "=========================================================" << endl;
    do {
        cout << "Barang ke - " << k<<endl;
        cout << "kode barang : "; cin.getline(brg[k].kd_brg, 12);
        cout << "Nama barang : "; cin.getline(brg[k].nm_brg, 40);
        cout << "Size Barang : "; cin.getline(brg[k].size, 10);
        cout << "Tanggal masuk : "; cin.getline(brg[k].tglmsk, 40);
        cout << "harga beli : Rp"; cin>>brg[k].hrg_beli;
        cout << "harga jual : Rp"; cin>>brg[k].hrg_jual;
        cout << "stok : "; cin>>brg[k].stok;
        cout << "\nKetik angka 1 untuk lanjut mengisi data barang baru, ketik angka 0 untuk berhenti : "; cin >> i;
        cout << endl;
        if (i == 1) {
            ulang = true;
            k += 1;
            cin.ignore();
        }
        else {
            ulang = false;
        }
    } while (ulang);
    cout << "=========================================================" << endl;
    cout << "                     Tampilan Output                     " << endl;
    cout << "=========================================================" << endl;
    for (int i = 1; i <= k; i++) {
        cout << "Barang ke - " << i << endl;
        cout << "kode barang : " << brg[i].kd_brg << endl;
        cout << "Nama barang : "<< brg[i].nm_brg <<endl;
        cout << "Size Barang : "<< brg[i].size<<endl;
        cout << "Tanggal masuk : "<<brg[i].tglmsk<<endl;
        cout << "harga beli : Rp"<<brg[i].hrg_beli<<endl;
        cout << "harga jual : Rp"<<brg[i].hrg_jual<<endl;
        cout << "stok : "<<brg[i].stok<<endl;
        cout << endl;
    }
    return 0;
}